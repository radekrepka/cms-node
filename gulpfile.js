const gulp = require('gulp');

gulp.task('keys', function() {
    gulp.src(['src/middleware/*.key'])
        // .pipe(chmod(666))
        .pipe(gulp.dest('build/middleware'), {"mode": "0666"});
    return 'Files with keys moved.';
});

gulp.task('default', gulp.series('keys', function() {
    return 'Complete';
}));
