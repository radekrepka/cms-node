import { Request, Response, Router } from 'express';
import {CheckAuth} from "../middleware/CheckAuth";

export default abstract class BaseRouter {

    public router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public routes() {
        this.router.get('/', this.all);
        this.router.get('/:_id', this.one);
        this.router.post('/', CheckAuth.checkAuth, this.create);
        this.router.put('/:_id', CheckAuth.checkAuth, this.update);
        this.router.delete('/:_id', CheckAuth.checkAuth, this.delete);
    }

    /**
     * @param req
     * @param res
     */
    public abstract all(req: Request, res: Response): void;

    /**
     * @param req
     * @param res
     */
    public abstract one(req: Request, res: Response): void;

    /**
     * @param req
     * @param res
     */
    public abstract create(req: Request, res: Response): void;

    /**
     * @param req
     * @param res
     */
    public abstract update(req: Request, res: Response): void;

    /**
     * @param req
     * @param res
     */
    public abstract delete(req: Request, res: Response): void;
}