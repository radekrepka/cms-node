import { Request, Response, Router } from 'express';
import BaseRouter from './BaseRouter';
import Article from '../model/schema/Article';

class ArticleRouter extends BaseRouter {

    public routes() {
        this.router.get('/without-bodies', this.allWithoutBodies);
        super.routes();
    }

    public all(req: Request, res: Response): void {
        Article.find()
            .then((data) => {
                let frontPage = {};
                data.map(article => {
                    if (article.frontPage) {
                        frontPage = article;
                    }
                });
                return res.status(200).json({ articles: data, frontPage });
            })
            .catch((error) => {
                res.status(500).json({ error });
                return error;
            });
    }

    public allWithoutBodies(req: Request, res: Response): void {
        Article.find()
            .select("-body")
            .then((data) => {
                let frontPage = {};
                data.map(article => {
                    if (article.frontPage) {
                        frontPage = article;
                    }
                });
                return res.status(200).json({ articles: data, frontPage });
            })
            .catch((error) => {
                res.status(500).json({ error });
                return error;
            });
    }

    public one(req: Request, res: Response): void {
        const { _id } = req.params;
        Article.findOne({ _id })
            .then((data) => {
                res.status(200).json({ data });
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }

    public create(req: Request, res: Response): void {
        const {
            title,
            body,
            img
        } = req.body;
        const article = new Article({
            title,
            body,
            img
        });
        article
            .save()
            .then((data) => {
                res.status(201).json({ data });
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }

    public update(req: Request, res: Response): void {
        const { _id } = req.params;
        Article.findOneAndUpdate({ _id }, req.body)
            .then((data) => {
                res.status(200).json({ data });
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }

    public delete(req: Request, res: Response): void {
        const { _id } = req.params;
        Article.findOneAndRemove({ _id })
            .then(() => {
                res.status(204).end();
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }
}

export default new ArticleRouter();