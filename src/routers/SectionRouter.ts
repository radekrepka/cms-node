import { Request, Response, Router } from 'express';
import * as S from 'string';
import BaseRouter from './BaseRouter';
import Section from '../model/schema/Section';

class SectionRouter extends BaseRouter {

    public routes() {
        this.router.get('/without-bodies', this.allWithoutBodies);
        this.router.get('/without-articles', this.allWithoutArticles);
        this.router.get('/:url/without-bodies', this.oneWithoutBodies);
        this.router.get('/:url', this.one);
        super.routes();
    }

    public all(req: Request, res: Response): void {
        Section.find()
            .populate("articles")
            .then((data) => {
                return res.status(200).json({ sections: data });
            })
            .catch((error) => {
                res.status(500).json({ error });
                return error;
            });
    }

    public allWithoutBodies(req: Request, res: Response): void {
        Section.find()
            .populate("articles", "-body")
            .then((data) => {
                return res.status(200).json({ sections: data });
            })
            .catch((error) => {
                res.status(500).json({ error });
                return error;
            });
    }

    public allWithoutArticles(req: Request, res: Response): void {
        Section.find()
            .select("-articles")
            .then((data) => {
                return res.status(200).json({ sections: data });
            })
            .catch((error) => {
                res.status(500).json({ error });
                return error;
            });
    }

    public one(req: Request, res: Response): void {
        const { url } = req.params;
        Section.findOne({ url })
            .populate("articles")
            .then((data) => {
                res.status(200).json({ section: data });
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }

    public oneWithoutBodies(req: Request, res: Response): void {
        const { url } = req.params;
        Section.findOne({ url })
            .populate("articles", "-body")
            .then((data) => {
                res.status(200).json({ section: data });
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }

    public create(req: Request, res: Response): void {
        const {
            name,
            sort
        } = req.body;
        const url = S(name).slugify().s;

        if (!sort || isNaN(sort)) {
            Section.findOne()
                .sort({sort: -1})
                .then((data) => {
                    const section = new Section({
                        name,
                        url,
                        sort: data.sort + 1
                    });
                    section
                        .save()
                        .then((data) => {
                            res.status(201).json({ data });
                        })
                        .catch((error) => {
                            res.status(500).json({ error });
                        });

                })
                .catch((error) => {
                    res.status(500).json({ error });
                });
        }
        else {
            const section = new Section({
                name,
                url,
                sort
            });
            section
                .save()
                .then((data) => {
                    res.status(201).json({ data });
                })
                .catch((error) => {
                    res.status(500).json({ error });
                });
        }
    }

    public update(req: Request, res: Response): void {
        const { _id } = req.params;
        Section.findOneAndUpdate({ _id }, req.body)
            .then((data) => {
                res.status(200).json({ data });
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }

    public delete(req: Request, res: Response): void {
        const { _id } = req.params;
        Section.findOneAndRemove({ _id })
            .then(() => {
                res.status(204).end();
            })
            .catch((error) => {
                res.status(500).json({ error });
            });
    }
}

export default new SectionRouter();