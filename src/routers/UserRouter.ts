import { Request, Response, Router } from 'express';
import slugify from 'slugify';
import * as bcrypt from 'bcrypt';
import BaseRouter from './BaseRouter';
import User from '../model/schema/User';
import {CheckAuth} from "../middleware/CheckAuth";

class UserRouter extends BaseRouter {

    public  routes() {
        super.routes();
        this.router.post('/login', this.login);
    }

    public all(req: Request, res: Response): void {
        return res.status(403);
        // User.find()
        //     .then((data) => {
        //         return res.status(200).json({ data });
        //     })
        //     .catch((error) => {
        //         res.status(500).json({ error });
        //         return error;
        //     });
    }

    public one(req: Request, res: Response): void {
        return res.status(403);
        // const { _id } = req.params;
        // User.findOne({ _id })
        //     .then((data) => {
        //         res.status(200).json({ data });
        //     })
        //     .catch((error) => {
        //         res.status(500).json({ error });
        //     });
    }

    public create(req: Request, res: Response): void {
        const {
            username,
            password
        } = req.body;

        User.find({username})
            .then((user) => {
                if (user.length >= 1) {
                    return res.status(409).json({message: "User exists"});
                }
                else {
                    bcrypt.hash(password, 10, (err, hash) => {
                        if (err) {
                            return res.status(500).json({
                                error: err
                            });
                        } else {
                            const user = new User({
                                username,
                                password: hash
                            });
                            user
                                .save()
                                .then(result => {
                                    console.log(result);
                                    res.status(201).json({
                                        message: "User created"
                                    });
                                })
                                .catch(err => {
                                    console.log(err);
                                    res.status(500).json({
                                        error: err
                                    });
                                });
                        }
                    });
                }
            });
    }

    public login (req: Request, res: Response): void {
        const {
            username,
            password
        } = req.body;

        User.find({ username })
            .exec()
            .then(user => {
                if (user.length < 1) {
                    return res.status(401).json({
                        message: "Auth failed"
                    });
                }
                bcrypt.compare(password, user[0].password, (err, result) => {
                    if (err) {
                        return res.status(401).json({
                            message: "Auth failed"
                        });
                    }
                    if (result) {
                        const token = CheckAuth.sign({username, password});
                        return res.status(200).json({
                            message: "Auth successful",
                            token: token
                        });
                    }
                    res.status(401).json({
                        message: "Auth failed"
                    });
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    error: err
                });
            });

    }

    public update(req: Request, res: Response): void {
        return res.status(403);
        // const { _id } = req.params;
        // User.findOneAndUpdate({ _id }, req.body)
        //     .then((data) => {
        //         res.status(200).json({ data });
        //     })
        //     .catch((error) => {
        //         res.status(500).json({ error });
        //     });
    }

    public delete(req: Request, res: Response): void {
        return res.status(403);
        // const { _id } = req.params;
        // User.findOneAndRemove({ _id })
        //     .then(() => {
        //         res.status(204).end();
        //     })
        //     .catch((error) => {
        //         res.status(500).json({ error });
        //     });
    }
}

export default new UserRouter();