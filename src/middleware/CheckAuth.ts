import * as fs from 'file-system';
import * as jwt from 'jsonwebtoken';

// use 'utf8' to get string instead of byte array  (512 bit key)
const privateKey  = fs.readFileSync(__dirname + '/private.key', 'utf8');
const publicKey  = fs.readFileSync(__dirname + '/public.key', 'utf8');

export module CheckAuth {
    export function sign (payload) : string {
        return jwt.sign(payload, privateKey, {
            expiresIn:  "30d",
            algorithm:  "RS256"
        });
    }

    export function verify(token) : boolean {
        try{
            return jwt.verify(token, publicKey, {
                expiresIn:  "30d",
                algorithm:  ["RS256"]
            });
        }
        catch (err){
            return false;
        }
    }

    export function decode(token) : string {
        //returns null if token is invalid
        return jwt.decode(token, {complete: true});
    }
    
    export function checkAuth(req, res, next) {
        try {
            const token = req.headers.authorization.split(" ")[1];
            const decoded = verify(token);
            req.userData = decoded;
            next();
        } catch (error) {
            return res.status(401).json({
                message: 'Auth failed'
            });
        }
    }
}