import { model, Schema } from 'mongoose';

const Article: Schema = new Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    img: {
        type: String,
        default: '',
        required: false
    },
    frontPage: {
        type: Boolean,
        default: false,
        required: false
    },
    dateAdd: {
        type: Date,
        default: Date.now(),
    },
});

export default model('Article', Article);