import { model, Schema } from 'mongoose';

const User: Schema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    dateAdd: {
        type: Date,
        default: Date.now(),
    },
});

export default model('User', User);