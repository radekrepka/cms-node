import { model, Schema } from 'mongoose';

const Section: Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        unique: true,
        required: true
    },
    sort: {
        type: Number,
        unique: true,
        required: true
    },
    articles: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Article',
        },
    ],
    dateAdd: {
        type: Date,
        default: Date.now(),
    },
});

export default model('Section', Section);