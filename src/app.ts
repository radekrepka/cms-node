import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import * as mongoose from 'mongoose';
import * as logger from 'morgan';
import * as config from 'config';
import articleRouter from './routers/ArticleRouter';
import sectionRouter from './routers/SectionRouter';
import userRouter from './routers/UserRouter';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    public config(): void {
        mongoose.connect(config.MONGO_URI, { useNewUrlParser: true });

        // express middleware
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(cookieParser());
        this.app.use(logger('dev'));

        this.app.use('/public', express.static('public'));

        // cors
        this.app.use((req, res, next) => {
            // res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
            res.header('Access-Control-Allow-Origin', '*');
            res.header(
                'Access-Control-Allow-Methods',
                'GET, POST, PUT, DELETE, OPTIONS',
            );
            res.header(
                'Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials',
            );
            res.header('Access-Control-Allow-Credentials', 'true');
            next();
        });
    }

    public routes(): void {
        const router: express.Router = express.Router();

        this.app.use('/', router);
        this.app.use('/api/articles', articleRouter.router);
        this.app.use('/api/sections', sectionRouter.router);
        this.app.use('/api/users', userRouter.router);
    }
}

export default new App().app;